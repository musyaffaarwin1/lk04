import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do { 
            try {
            System.out.print("Masukkan pembilang: ");
            int pembilang = scanner.nextInt();

            System.out.print("Masukkan penyebut: ");
            int penyebut = scanner.nextInt();

            int hasil = pembagian(pembilang, penyebut);
            System.out.println("Hasil pembagian: " + hasil);

            validInput = true;

        } catch (InputMismatchException e) {
            System.out.println("Input harus berupa bilangan bulat. Silakan coba lagi.");
            scanner.nextLine(); 
        } catch (ArithmeticException e) {
            System.out.println(e);
        }     
    
    } while (!validInput);

        scanner.close();
    }

    public static int pembagian(int pembilang, int penyebut) {
        //add exception apabila penyebut bernilai 0
        if (penyebut == 0) {
            throw new ArithmeticException("Penyebut tidak boleh angka 0."); }

        return pembilang / penyebut;
    }
}
